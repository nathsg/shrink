module.exports = {
  siteMetadata: {
    title: `A shortner url site`,
    author: `n-th`,
    description: `This is a website for shortening your url.`,
    subtitle: `Made with love`,
    siteUrl: `https://shrink.n-th.me`,
    lang: 'en-US'
  },
  plugins: [
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/app/*`] },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-174224580-1`,
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-robots-txt`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/images/`,
      },
    }
  ],
}
