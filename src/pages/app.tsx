import React from 'react'
import { Router } from "@reach/router"

import Redirect  from '../components/redirect';

export default function AppPage() {
  return(
    <Router>
      <Redirect path="/app/:url" />
    </Router>
  )
}
