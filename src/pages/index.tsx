import React, { useState, useEffect } from "react"
import styled from "styled-components"
import axios from "axios"

import SEO from "../components/seo"
import Hero from "../components/hero"
import List from "../components/list"
import Footer from "../components/footer"
import Layout from "../components/layout"
import Loader from "../components/loader"
import { API_URL } from '../utils'

const StyledDiv = styled.div`
  color: #605c5c;
  text-align: center;
  padding-top: 45px;
  font-size: 40px;
  margin-bottom: 100px;
`

const StyledP = styled.p`
  margin-bottom: 80px;
`

export default function HomePage() {
  let fetchData = React.useCallback(async () => {
      axios.get(API_URL)
        .then(response => {
          setList(response.data)
          setLoading(false)
        })
  },[])

  useEffect(() => {
    fetchData();
    },[fetchData]);

  const [list, setList] = useState([])
  const [isLoading, setLoading] = useState(true)

  return (
    <Layout>
      <SEO title="Shrink" />
      <Hero fetchData={fetchData}/>
      <StyledDiv>
        <StyledP>Top 5</StyledP>
        {isLoading && <Loader/>}
        {list && <List linkData={list} />}
      </StyledDiv>
      <Footer />
    </Layout>
  )
}
