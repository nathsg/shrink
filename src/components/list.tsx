import React from "react"
import styled from "styled-components"

import { BASE_URL } from '../utils'

const StyledList = styled.ol`
  text-align: center;
  font-size: 20px;
  margin: 0 15%;

  @media(max-width: 768px) {
    margin: 0 5%;
  }
`

const StyledLi = styled.li`
  display: flex;
  justify-content: space-between;
  padding: 15px;
  border-bottom: 1px solid #fff0f2;
`

const StyledA = styled.a`
  color: #605c5c;
  text-decoration: none;
`

const List = ({ linkData }) => {
  return (
    <StyledList data-cy="ul-shrink">
      {linkData.map(link => (<Li key={link.short_uri} shortUri={link.short_uri} count={link.count}/>)) }
    </StyledList>
  )
}

const Li = ({shortUri, count}) => {
  const shortUrl = `${BASE_URL}/${shortUri}`;

  return (
    <StyledLi
      data-cy="li-shrink"
      key={shortUri}
    >
      <StyledA data-cy="a-shrink" href={`https://${shortUrl}`}> {shortUrl} </StyledA>
      <p data-cy="p-shrink" > {count} </p>
    </StyledLi>
  )
}

export default List
