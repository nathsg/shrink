import React from "react"
import styled from "styled-components"

const StyledDiv = styled.div`
position: absolute;
width: 100px;
height: 50px;
top: 50%;
left: 50%;
margin-left: -50px;
margin-top: -25px;
font-size: 50px;
font-family: -apple-system, BlinkMacSystemFont, sans-serif;
color: #605c5c;
`

const Error = () => {
    return (
    <StyledDiv>
        Oops!
    </StyledDiv>
    )
}

export default Error
