import React from "react"
import styled from "styled-components"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const StyledFooter = styled.div`
  display: flex;
  color: #605c5c;
  margin: 20px 40px;
  justify-content: space-between;
  font-size: 20px;
  position: relative;
  bottom: 0;

  @media(max-width: 375px) {
    justify-content: center;
  }
`

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
`

const StyledHeartDiv = styled.div`
  margin: 0 5px;
`

const StyledA = styled.a`
  color: #605c5c;
  text-decoration: none;
  margin-left: 3px;
`

const StyledLogo = styled.div`
  @media(max-width: 375px) {
    display: none;
  }
`

const Footer = () => {
  const data = useStaticQuery(graphql`
    query {
      heart: file(relativePath: { eq: "heart-emoji.png" }) {
        childImageSharp {
          fixed(width: 20, height: 20) {
            ...GatsbyImageSharpFixed
          }
        }
      }

      twitter: file(relativePath: { eq: "twitter-logo.png" }) {
        childImageSharp {
          fixed(width: 50, height: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return (
    <StyledFooter>
      <StyledDiv>
        <p> Made with </p>
        <StyledHeartDiv>
          <Img fixed={data.heart.childImageSharp.fixed} alt="Heart icon" />
        </StyledHeartDiv>
        <p> by </p>
        <StyledA
          rel="noopener noreferrer"
          target="_blank"
          href="https://www.interlink.com.ar/"
        >
          Interlink
        </StyledA>
      </StyledDiv>
      <StyledLogo>

      <StyledA
          rel="noopener noreferrer"
          target="_blank"
          href="https://twitter.com/"
        >
        <Img fixed={data.twitter.childImageSharp.fixed} alt="Twitter" />
        </StyledA>
      </StyledLogo>
    </StyledFooter>
  )
}

export default Footer
