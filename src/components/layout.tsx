import React, { ReactNode } from "react"
import reset from "react-style-reset/string"
import styled, { createGlobalStyle } from "styled-components"

const GlobalStyles = createGlobalStyle`
  ${reset};
`

const StyledDiv = styled.div`
  min-height: 100vh;
  font-family: -apple-system, BlinkMacSystemFont, sans-serif;
`

interface Props {
  children: ReactNode
}

const Layout = ({ children }: Props) => {
  return (
    <StyledDiv>
      <GlobalStyles />
      <main>{children}</main>
    </StyledDiv>
  )
}

export default Layout
