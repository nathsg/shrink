import React, { useEffect } from 'react'

import { API_URL } from '../utils'

const Redirect = (props) => {
    const uri: string = props.url

    useEffect(()=> {
      fetch(`${API_URL}${uri}`)
        .then(response => response.json())
        .then(data => {
          const urlData = data[0]
          console.log('vai desgraça')
          if (urlData) {
            redirect(urlData)
          }
        })
    }, [])

    const redirect = (urlData) => {
      const longUrl = urlData.long_url
      window.location.replace(`${longUrl}`)
    }

    return <div></div>
}

export default Redirect
