import React, { useState } from "react"
import styled from "styled-components"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import BackgroundImage from "gatsby-background-image"
import axios from 'axios'
import { CopyToClipboard } from 'react-copy-to-clipboard'

import { API_URL, BASE_URL } from '../utils'

const StyledDiv = styled.div`
  color: #f9f2f2;
  text-align: center;
  padding: 60px 50px 0;
  background-color: rgba(255, 240, 242, 0.1);

  @media(max-width: 768px) {
    background-color: rgba(0, 0, 0, 0.6);
  }
`

const StyledH1 = styled.h1`
  font-size: 45px;
  margin-bottom: 10px;
`

const StyledP = styled.p`
  font-size: 20px;
  margin-bottom: 50px;

  @media(max-width: 768px) {
    font-size: 15px;
  }
`

const StyeldForm = styled.form`
  text-align: center;
  padding-bottom: 160px;
`

const StyledLogo = styled.div`
  position: absolute;
  top: 15px;
  left: 90px;

  @media(max-width: 768px) {
    position: initial;
    margin-bottom: 40px;
  }
`

const StyledInput = styled.input`
  height: 35px;
  padding-left: 15px;
  border: none;
  border-top-left-radius: 2px;
  border-bottom-left-radius: 2px;
  outline: none;
  width: 45%;
  font-size: 16px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  &:focus::placeholder {
    color: transparent;
  }
`

const StyledButton = styled.button`
  height: 37px;
  text-transform: uppercase;
  border: none;
  border-top-right-radius: 2px;
  border-bottom-right-radius: 2px;
  background-color: #f6435d;
  color: #f9f2f2;
  outline: none;
  font-size: 16px;

  &:hover {
    cursor: pointer;
  }
`

const StyledRefreshButton = styled.button`
  position: absolute;
  background: none;
  outline: none;
  border: none;

  &:hover {
    cursor: pointer;
  }
`

const Hero = ({fetchData}) => {

  const data = useStaticQuery(graphql`
    query {
      interlink: file(relativePath: { eq: "interlink-logo-white.png" }) {
        childImageSharp {
          fixed(width: 90, height: 90) {
            ...GatsbyImageSharpFixed
          }
        }
      }

      refresh: file(relativePath: { eq: "refresh.png" }) {
        childImageSharp {
          fixed(width: 30, height: 30) {
            ...GatsbyImageSharpFixed
          }
        }
      }

      background: file(relativePath: { eq: "background.jpg" }) {
        childImageSharp {
          fluid(quality: 90, maxWidth: 1920) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `)

  const [input, setInput] = useState("")
  const [shortUrl, setShortUrl] = useState("")

  const handleSubmit = e => {
    e.preventDefault()

    if (urlValidation(input)) {
      setShortUrl("")
      axios.post(API_URL, { longUrl: input})
        .then(response => {
          setInput('')
          setShortUrl(`${BASE_URL}/${response.data.short_uri}`)
          fetchData()
        })
    } else {
      alert('Wrong url format!')
    }

  }

  const urlValidation = (text: string) => {
    const expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi
    var regex = new RegExp(expression)

    return text.match(regex)
  }

  const handleInputChange = (e: any) => {
    setInput(e.target.value)
  }

  const resetShorUrl = () => {
    setShortUrl("")
  }

  const inputSelection = () =>{
    if (shortUrl) {
      return <CopyBoard value={shortUrl} resetShorUrl={resetShorUrl} image={data.refresh.childImageSharp.fixed}/>
    }

    return <Form handleSubmit={handleSubmit} input={input} handleInputChange={handleInputChange}/>
  }

  return (
    <BackgroundImage fluid={data.background.childImageSharp.fluid}>
      <StyledDiv>
        <StyledLogo>
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://www.interlink.com.ar/"
          >
            <Img
              fixed={data.interlink.childImageSharp.fixed}
              alt="Interlink logo"
            />
          </a>
        </StyledLogo>
        <StyledH1> Shrink your link!</StyledH1>
        <StyledP>
          A long URL is always a problem. It's hard to remember and share.
        </StyledP>
        {inputSelection()}
      </StyledDiv>
    </BackgroundImage>
  )
}

const CopyBoard = ({ value, resetShorUrl , image }) => {
  return (
    <StyeldForm data-cy="form-copyboard" >
      <StyledInput data-cy="input-copyboard" value={value} readOnly/>
      <CopyToClipboard text={value}>
        <StyledButton
          data-cy="button-copyboard"
          type="button" onClick={resetShorUrl}>Copy</StyledButton>
      </CopyToClipboard>
      <StyledRefreshButton
        data-cy="refresh-shrink"
        type="button"
        onClick={resetShorUrl}
      >
        <Img
          fixed={image}
          alt="Refresh button"
        />
      </StyledRefreshButton>
    </StyeldForm >
  )
}

const Form = ({ handleSubmit, input, handleInputChange }) => {
  return(
    <StyeldForm
      data-cy="form-shrink"
      onSubmit={handleSubmit}
    >
      <StyledInput
        data-cy="input-shrink"
        type="text"
        placeholder="Paste the link to shrink it"
        value={input}
        onChange={handleInputChange}
      />
      <StyledButton
        data-cy="submit-shrink"
        type="submit"
      >
        Shrink
      </StyledButton>
    </StyeldForm>
  )
}

export default Hero
