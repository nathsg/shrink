/// <reference types="cypress" />

context('Shrink page', () => {
    beforeEach(()=>{
        cy.visit('/');
    })

    it('should render the home page"', () => {
        cy.get('h1').should('exist')
    })
})

context('Shrink form', () => {
    beforeEach(()=>{
        cy.server();
        cy.route('**/prod/**', 'fixture:url.json')

        cy.visit('/');
    })

    it('should have an input for text', () => {

        cy.get('[data-cy=form-shrink]').should('exist');

        cy.get('[data-cy=input-shrink]').should('exist');
    })


    it('should show a validation feedback for wrong url format', () => {
        const stub = cy.stub()

        cy.on('window:alert', stub)

        cy.get('[data-cy=input-shrink]')
        .type('notanurl')
        .type('{enter}')
        .then(()=> {
            expect(stub.getCall(0)).to.be.calledWith('Wrong url format!')
        })

        cy.get('button').click()
        .then(()=> {
            expect(stub.getCall(0)).to.be.calledWith('Wrong url format!')
        })
    })

    it('should have button "Shrink"', () => {
        cy.get('[data-cy=submit-shrink]').should('contain', 'Shrink');
    })

    it('should show link to copy after a successful long url submition', () => {
        const longUrl = ""
        cy.server();
        cy.route({
          url: '**/prod/**',
          method: 'POST',
          status: 200,
          response: 'fixture:long-url.json'
        });

        cy.route({
            url: '**/prod/**',
            method: 'GET',
            status: 200,
            response: 'fixture:short-url.json'
          });

        cy.get('[data-cy=input-shrink]')
          .type('https://google.com.br')

        cy.get('[data-cy=form-shrink]')
          .submit()
          .wait(1000)

          cy.get('[data-cy=form-shrink]').should('not.exist');
          cy.get('[data-cy=input-shrink]').should('not.exist');

          cy.get('[data-cy=form-copyboard]').should('exist');
          cy.get('[data-cy=button-copyboard]').should('exist');
    })
})

context('Shrink list', () => {
    beforeEach(()=>{
        cy.server();
        cy.route('**/prod/**', 'fixture:url.json')

        cy.visit('/');
    })

    it('should be not exist if there are no url', () => {
        cy.route('**/prod/**', [])
        cy.get('[data-cy=ul-shrink]').should('not.exist')
    })

    it('should show a list with url and count', () => {
        cy.get('[data-cy=li-shrink]').should('exist')
        cy.get('[data-cy=a-shrink]').should('exist')
        cy.get('[data-cy=p-shrink]').should('exist')
    })

    it('should show less than 6 itens', () => {
        cy.get('[data-cy=ul-shrink').find('li').its('length').should('be.lte', 6)
    })
})
