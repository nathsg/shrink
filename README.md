# This is Shrink UI

[Shrink](https://shrink.n-th.me) is a project for shortening url. It is made with CSS, HTML, JavaScript and love.

#

#### Requirements
 - npm v6.14.7
 - Gatsby CLI v2.12.68
 - Gatsby v2.24.23
 - AWS CLI v2

#

## How to run

After cloning this project, go to your root folder and install the dependencies using your package manager for JavaScript.

```
npm install
```


### Gatsby server
If you'd like to run Gatsby's server in development mode, use this command:

```
npm run start
```

### Cypress server

If you want to run our tests in a browser, use this command:
```
npm run cy:open
```

or, type

```
npm run cy:run
```

to run tests on the terminal.

***It is necessary to run gatsby server locally to execute tests!***

#

## How to deploy

### Gatsby server

If you'd like to build Gatsby's server in production mode, use this command:

```
npm build
```

It will create a public folder for Shrink static site.

### AWS deploy

After installing aws cli, and setting aws configuration we need to create our stack with cloudformation's files.

```
aws cloudformation create-stack --stack-name shrink-frontend --template-body file://.aws/root.template.yaml --region sa-east-1  --profile n-th
```

To upload files in S3, run

```
aws s3 sync public s3://shrink.n-th.me --profile n-th
```

***Note that the configuration present in Cloudformation are for Shrink project!***

#

## Cool stuff
Here are some technologies used in this project:

- Gatsby
- AWS (S3 + CloudFront + Route53 + Certification Manager + CloudFormation + CodeBuild + CloudWatch)
- Cypress
- Google Analytics
- Styled Components

#

Why Gatsby? Why S3? Check our documentation [here](https://www.notion.so/b2c3ead850d249858de3989683be623c?v=58d3396e28a3421b97db073906a67ec7).


Also, check [Shrink serverless repository](https://gitlab.com/nathsg/shrink-serverless).
